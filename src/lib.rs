/*!
# cons
[![crates.io](https://img.shields.io/crates/v/pour)](https://crates.io/crates/cons)
[![Downloads](https://img.shields.io/crates/d/pour)](https://crates.io/crates/cons)
[![Documentation](https://docs.rs/pour/badge.svg)](https://docs.rs/cons/)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Quick and simple hash-consing implementations for a variety of internally mutable `HashMap`s/`HashSet`s (use the features to select the ones you want).
*/
use std::rc::Rc;
use std::sync::Arc;

/// A trait implemented by sets which can be used for hash-consing values of type `C` using keys of type `Q`
pub trait Cons<C, Q> {
    /// Cons a key of type `Q` into a cached key of type `C`
    fn cons(&self, key: Q) -> C;
}

/// A trait implemented by values which can be garbage collected
pub trait CanGC {
    /// Whether this value only has a single reference to it, and therefore can be garbage collected
    fn single_reference(&self) -> bool;
    /// Whether this value has two or less references to it, and therefore can be garbage collected by an object holding
    /// the other reference
    fn double_reference(&self) -> bool;
}

/// A trait implemented by sets which can be used to un-cons values of type `C`
pub trait Uncons<C> {
    /// Garbage collect a value of type `C`, removing it if there cannot be any other references to it.
    /// If this is the case, return `true`, otherwise return `false`.
    #[inline(always)]
    fn gc(&self, key: &mut C) -> bool
    where
        C: CanGC,
    {
        if key.double_reference() {
            self.uncons(key)
        } else {
            false
        }
    }
    /// Unconditionally remove a value of type `C`. Return whether it was in the set
    fn uncons(&self, key: &mut C) -> bool;
}

impl<C> CanGC for Arc<C> {
    #[inline]
    fn single_reference(&self) -> bool {
        Arc::weak_count(self) + Arc::strong_count(self) <= 1
    }
    #[inline(always)]
    fn double_reference(&self) -> bool {
        Arc::weak_count(self) + Arc::strong_count(self) <= 2
    }
}

impl<C> CanGC for Rc<C> {
    #[inline]
    fn single_reference(&self) -> bool {
        Rc::weak_count(self) + Rc::strong_count(self) <= 1
    }
    #[inline(always)]
    fn double_reference(&self) -> bool {
        Rc::weak_count(self) + Rc::strong_count(self) <= 2
    }
}

#[cfg(feature = "elysees")]
mod elysees_impl;
#[cfg(feature = "flurry-cons")]
pub mod flurry_cons;
