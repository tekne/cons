use super::*;

impl<C> CanGC for elysees::Arc<C> {
    /// Whether this value only has a single reference to it, and therefore can be garbage collected
    fn single_reference(&self) -> bool {
        self.is_unique()
    }
    /// Whether this value has two or less references to it, and therefore can be garbage collected by an object holding
    /// the other reference
    fn double_reference(&self) -> bool {
        elysees::Arc::count(self, std::sync::atomic::Ordering::Acquire) <= 2
    }
}
