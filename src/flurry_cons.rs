/*!
A hash-consing implementation using `flurry`
*/
use super::*;
use ahash::RandomState;
use flurry::HashMap;
use std::borrow::Borrow;
use std::hash::{BuildHasher, Hash};

/// A cache for hash-consed values implemented using `flurry`.
#[derive(Debug)]
pub struct Cache<C, S = RandomState> {
    set: HashMap<C, (), S>,
}

impl<C, S: Default> Cache<C, S> {
    pub fn default() -> Cache<C, S> {
        Cache {
            set: HashMap::default(),
        }
    }
}

impl<C, S> Cache<C, S> {
    /// Create a new, empty `Cache` with the given hasher
    pub fn with_hasher(hash_builder: S) -> Cache<C, S> {
        Cache {
            set: HashMap::with_hasher(hash_builder),
        }
    }
    /// Create a new, empty `Cache` with the given capacity and hasher
    pub fn with_capacity_and_hasher(capacity: usize, hash_builder: S) -> Cache<C, S> {
        Cache {
            set: HashMap::with_capacity_and_hasher(capacity, hash_builder),
        }
    }
    /// Get the number of elements cached
    pub fn len(&self) -> usize {
        self.set.len()
    }
}

impl<C> Cache<C> {
    /// Create a new, empty `Cache` with no capacity and the default hasher
    pub fn new() -> Cache<C> {
        Cache::default()
    }
    /// Create a new, empty `Cache` with a given capacity and the default hasher
    pub fn with_capacity(capacity: usize) -> Cache<C> {
        Cache::with_capacity_and_hasher(capacity, RandomState::default())
    }
}

impl<C, Q, S> Cons<C, Q> for Cache<C, S>
where
    C: 'static + Hash + Ord + Send + Sync + Clone + Borrow<Q>,
    Q: Hash + Ord + Into<C>,
    S: BuildHasher,
{
    fn cons(&self, key: Q) -> C {
        let guard = self.set.guard();
        if let Some((key, _value)) = self.set.get_key_value(&key, &guard) {
            key.clone()
        } else {
            let key: C = key.into();
            while let Err(_) = self.set.try_insert(key.clone(), (), &guard) {
                if let Some((key, _value)) = self.set.get_key_value::<C>(&key, &guard) {
                    return key.clone();
                }
            }
            key.clone()
        }
    }
}

impl<C, S> Uncons<C> for Cache<C, S>
where
    C: 'static + Hash + Ord + Send + Sync + Clone,
    S: BuildHasher,
{
    fn uncons(&self, key: &mut C) -> bool {
        let guard = self.set.guard();
        self.set.remove(&key, &guard).is_some()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_consing_test() {
        let cache = Cache::<Arc<u64>>::new();
        let mut x = cache.cons(1);
        let mut y = cache.cons(2);
        let z = cache.cons(1);
        assert_eq!(*x, 1);
        assert_eq!(*y, 2);
        assert_eq!(*z, 1);
        assert_eq!(Arc::as_ptr(&x), Arc::as_ptr(&z));
        assert!(y.double_reference());
        assert!(!y.single_reference());
        assert!(!x.double_reference());
        assert!(!cache.gc(&mut x));
        assert!(cache.gc(&mut y));
        let y2 = cache.cons(2);
        assert_ne!(Arc::as_ptr(&y), Arc::as_ptr(&y2));
    }
}