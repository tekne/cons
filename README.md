# cons
[![crates.io](https://img.shields.io/crates/v/pour)](https://crates.io/crates/cons)
[![Downloads](https://img.shields.io/crates/d/pour)](https://crates.io/crates/cons)
[![Documentation](https://docs.rs/pour/badge.svg)](https://docs.rs/cons/)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

Quick and simple hash-consing implementations for a variety of internally mutable `HashMap`s/`HashSet`s (use the features to select the ones you want).